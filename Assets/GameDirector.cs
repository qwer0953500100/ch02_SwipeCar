﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDirector : MonoBehaviour
{
    float speed = 0;
    Vector2 startPos;
    GameObject car;
    GameObject flag;
    GameObject distance;
    // Start is called before the first frame update
    void Start()
    {
        this.car = GameObject.Find("汽車");
        this.flag = GameObject.Find("旗子");
        this.distance = GameObject.Find("Distance");
    }

    // Update is called once per frame
    void Update()
    {
        

        if(Input.GetMouseButtonDown(0)) {
            this.startPos = Input.mousePosition;
        } else if(Input.GetMouseButtonUp(0)) {
            Vector2 endPos = Input.mousePosition;
            float swipeLength =endPos.x -this.startPos.x;
            
            this.speed = swipeLength/500.0f;
            
            this.car.GetComponent<AudioSource>().Play();
        }

        transform.Translate(this.speed, 0, 0);
        this.speed *=0.98f;
        
        float l = this.flag.transform.position.x - this.car.transform.position.x;
        if(l >= 0)
        {
            this.distance.GetComponent<Text>().text = "距離目標還有" + l.ToString("F2") + "m";
        }
        else
        {
            this.distance.GetComponent<Text>().text ="抵達終點 ! ";
　　　　 }
    }
}    

